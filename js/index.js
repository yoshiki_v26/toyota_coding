$(function() {
    // スマホ最大幅
    const smWidth = 768;
    // 現在の幅
    var $windowWidth = $(window).width();

    // slickスライダーの動作
    $('.kv_slider').slick({
        autoplay: true,
        autoplaySpeed: 5000,
        fade: true,
        arrows: false,
        pauseOnHover: false
    });
    // リサイズ時の処理
    $(window).resize(function(){
        // ヘッダー位置がずれないように
        $windowWidth = $(window).width();
        headerHight = $('header').height();
        if($windowWidth > smWidth) {
            $('.menu').css('display', 'flex');
            $('.ham_line').removeClass('active');
        }
        else if (!$('.ham_line').hasClass('active')){
            $('.menu').css('display', 'none');
        }
    });
    // ハンバーガーメニュー
    $('.menu_ham').click(function(){
        if($('.ham_line').hasClass('active')) {
            $('.ham_line').removeClass('active');
            $('.menu').slideUp();
        }
        else {
            $('.ham_line').addClass('active');
            $('.menu').slideDown();
        }
    });
    // スマホサイズ時のメニューのページ内リンクを押した時の処理
    $('.menu a').click(function(){
        var $windowWidth = $(window).width();
        if(!($windowWidth > smWidth)) {
            $('.ham_line').removeClass('active');
            $('.menu').slideUp();
        }
    });
    // ページ内リンクの動作
    $('a[href^="#"]').click(function(){
        const speed = 400;
        var headerHight = $('header').outerHeight();
        var href= $(this).attr("href");
        var target = $(href == "#" || href == "" ? 'html' : href);
        var position;
        position = target.offset().top - headerHight;
        $('body,html').animate({scrollTop:position}, speed, 'swing');
        return false;
    });

    // モーダル処理
    var $modal;
    $('.shop_list .item').click(function(){
        var index = $('.item').index(this);
        $modal = $('.modals .modal').eq(index);
        $modal.fadeIn();
        $('#overlay').fadeIn();
    });
    $('#overlay, .close').click(function(){
        $modal.fadeOut();
        $('#overlay').fadeOut();
    });
});